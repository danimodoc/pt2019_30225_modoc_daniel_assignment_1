package View;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import model.CalcPolyModel;

public class CalcPolyView extends JFrame {

	private JTextField userInput1 = new JTextField(35);
	private JTextField userInput2 = new JTextField(35);
	private JTextField output = new JTextField(35);
	//private JTextField m_totalTf = new JTextField(20);
	private JButton addBtn = new JButton("Add");
	private JButton subBtn = new JButton("Subtract"); //////////// **
	private JButton multiplyBtn = new JButton("Multiply");
	private JButton divideBtn = new JButton("Divide");
	private JButton diffBtn = new JButton("Differentiate First");
	private JButton integBtn = new JButton("Integrate First");
	//private JButton clearBtn = new JButton("Clear");
	
	CalcPolyModel model;
	
	public CalcPolyView(CalcPolyModel modelo) {
		model =modelo;
		
		JFrame f = new JFrame ("Polynomial Calculator");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(700,500); 
		
		JPanel content1 = new JPanel();
		content1.setLayout(new FlowLayout());
		content1.add(new JLabel("Polynomial 1"));
		content1.add(userInput1);
		
		JPanel content12 = new JPanel();
		content12.add(new JLabel("For sharp results insert a polynomial like this: +-ax^b +-cx^d +-ex^f +-...\n Space is mandatory after each monomial ."));
		//content12.add(userInput1);
		
		JPanel content2 = new JPanel();
		content2.add(new JLabel("Polynomial 2"));
		content2.add(userInput2);
		content2.setLayout(new FlowLayout());
		
		JPanel content3 = new JPanel();
		content3.add(new JLabel("Result            "));
		content3.add(output);
		content3.setLayout(new FlowLayout());
		
		JPanel content4 = new JPanel();
		content4.add(addBtn);
		content4.add(subBtn);
		content4.add(multiplyBtn);
		content4.add(divideBtn);
		content4.add(diffBtn);
		content4.add(integBtn);
		content3.setLayout(new FlowLayout());
		
		JPanel ContentAll = new JPanel();
		ContentAll.add(content12);
		ContentAll.add(content1);
		ContentAll.add(content2);
		ContentAll.add(content3);
		ContentAll.add(content4);
		ContentAll.setLayout(new BoxLayout(ContentAll,BoxLayout.Y_AXIS));
		
		f.setContentPane(ContentAll);
		f.setVisible(true);
	}
	
	void reset() {
		userInput1.setText(CalcPolyModel.INITIAL_VALUE);
		userInput2.setText(CalcPolyModel.INITIAL_VALUE);
		output.setText(CalcPolyModel.INITIAL_VALUE);
	}
	
	public String getUserInput1() {

		return userInput1.getText();
	}
	
	public String getUserInput2() {

		return userInput2.getText();
	}
	
	public void addListener(ActionListener mal) {
		addBtn.addActionListener(mal);
	}
	
	public void subListener(ActionListener mal) {
		subBtn.addActionListener(mal);
	}
	
	public void multiplyBtnListener(ActionListener mal) {
		multiplyBtn.addActionListener(mal);
	}
	
	public void divideBtnListener(ActionListener mal) {
		divideBtn.addActionListener(mal);
	}
	
	public void diffBtnListener(ActionListener mal) {
		diffBtn.addActionListener(mal);
	}
	
	public void integBtnListener(ActionListener mal) {
		integBtn.addActionListener(mal);
	}
	
	public void setTotal(String newTotal) {
		output.setText(newTotal);
	}
	
	public void showError(String errMessage) {
		JOptionPane.showMessageDialog(this, errMessage);
	}

}
