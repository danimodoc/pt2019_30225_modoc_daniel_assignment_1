package model;

import Controller.CalcPolyController;
import View.CalcPolyView;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Polynomial p1 = new Polynomial("1x^2 -2x^1 +1x^0 ");//
		//3x^3 +4x^2 -3x^1 -7x^0
		//5x^4 -3x^3 +2x^2 -8x^0
		
		//Polynomial p2=new Polynomial("1x^1 -1x^0 ");		
				
		CalcPolyModel model = new CalcPolyModel();
	    CalcPolyView view = new CalcPolyView(model);
	    CalcPolyController controller = new CalcPolyController(model, view);
	    //view.setVisible(true);
	   
	}
}
