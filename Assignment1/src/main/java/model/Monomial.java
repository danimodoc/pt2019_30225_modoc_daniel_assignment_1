package model;

/**
 * @author danimodoc Clasa Monomial reprezinta un monom . Mai multe monoame
 *         adunate constituie un polinom . Monomul are caracteristici principale
 *         gradul si coeficientul .
 */
public class Monomial implements Comparable<Monomial> {

	protected double coef;
	protected int grad;

	public Monomial() {
	}

	public Monomial(double coef, int grad) {
		this.coef = coef;
		this.grad = grad;
	}
	
	/**
	 * Setere si getere pentru un obiect de tip monom .
	 * 
	 */

	public int getGrad() {
		return this.grad;
	}
	
	public double getCoef() {
		return this.coef;
	}
	
	public void setGrad(int grad) {
		this.grad = grad;
	}
	
	public void setCoef(int coef) {
		this.coef = coef;
	}

	public void showM() {
		System.out.println(this.coef + "x^" + this.grad);
	}

	public Monomial addMono(Monomial m) {// Rezultatul il memoram in primul monom
		Monomial newm = new Monomial(0, 0);
		if (this.grad == m.grad) {
			newm.coef = this.coef + m.coef;
			newm.grad = this.grad;
		}
		return newm;
	}
	
	/**
	 * Se efectueaza scaderea a doua monoame
	 */

	public Monomial subMono(Monomial m) {
		Monomial newm = new Monomial(0, 0);
		if (this.grad == m.grad) {
			newm.coef = this.coef - m.coef;
			newm.grad = this.grad;
		}
		return newm;
	}

	/**
	 * Se efectueaza inmultirea a doua monoame
	 */
	public Monomial times(Monomial m) {
		Monomial n = new Monomial(this.coef * m.coef, this.grad + m.grad);
		return n;
	}
	
	/**
	 * Se efectueaza impartirea a doua monoame
	 */
	public Monomial divideBy(Monomial m) {
		Monomial n=new Monomial(1.0*this.coef/m.coef,this.grad - m.grad);
		return n;
	}

	/**
	 * Se efectueaza integrarea unui monom
	 */
	public void integrate() {
		this.coef =((double) this.coef) / (this.grad + 1);
		this.grad = this.grad + 1;
	}

	/**
	 * Implementarea metodei compareTo deoarece in Polynomial dorim
	 * sa sortam lista de monoame a polinomului dupa grad .
	 */
	public int compareTo(Monomial m) {
		int grad1 = ((Monomial) m).getGrad();
		return grad1 - this.grad;
	}

}
