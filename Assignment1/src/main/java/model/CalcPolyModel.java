package model;

import javax.swing.JOptionPane;

public class CalcPolyModel {
	public static final String INITIAL_VALUE = "";

	protected String output;

	CalcPolyModel() {
		reset();
	}
	
	public void reset() {
		// input1=INITIAL_VALUE;
		// input2=INITIAL_VALUE;
		output = INITIAL_VALUE;
	}
	
	/**
	 * 6 metode care realizeaza operatiile necesare asupra polinoamelor construite din 
	 * inputurile introduse de utilizator . Operatiile folosite sunt cele din clasa Polynomial .
	 */

	public void addInputs(String input1, String input2) {
		
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0)
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input1);
			return ;
		}
		else if(input2.indexOf(' ') < 0 || input2.indexOf('x') < 0 || input2.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input2);
			return ;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p2 = new Polynomial(input2);
		Polynomial p3 = new Polynomial();
		
		if (p1.polygrad > p2.polygrad)
			p3 = p1.addPoly1(p2);
		else
			p3 = p2.addPoly1(p1);

		output = p3.toString();
	}

	public void subInputs(String input1, String input2) {
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0)
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input1);
			return;
		}
		else if(input2.indexOf(' ') < 0 || input2.indexOf('x') < 0 || input2.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input2);
			return;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p2 = new Polynomial(input2);
		Polynomial p3 = new Polynomial();
		
		if (p1.polygrad > p2.polygrad)
			p3 = p1.subPoly1(p2);
		else
			p3 = p1.subPoly2(p2);

		output = p3.toString();
	}

	public void multInputs(String input1, String input2)  {
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0)
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input1);
			return;
		}
		else if(input2.indexOf(' ') < 0 || input2.indexOf('x') < 0 || input2.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input2);
			return;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p2 = new Polynomial(input2);
		Polynomial p3 = new Polynomial();
		p3 = p1.timesPoly(p2);
		
		output = p3.toString();
	}
	
	public void divInputs(String input1, String input2) {
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0)
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input1);
			return ;
		}
		else if(input2.indexOf(' ') < 0 || input2.indexOf('x') < 0 || input2.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :"+input2);
			return ;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p2 = new Polynomial(input2);
		Polynomial[] p3 = new Polynomial[2];
		
		p3 = p1.divide(p2);
		output = p3[0].convertPolyString2()+" + ("+p3[1].convertPolyString2()+") / ("+p2.toString()+")";
	}

	public void diffInput(String input1) {
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :");
			return;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p3 = new Polynomial();
		
		p3 = p1.derivative();
	    output = p3.toString();
	}

	public void integInput(String input1) {
		if(input1.indexOf(' ') < 0 || input1.indexOf('x') < 0 || input1.indexOf("^") < 0 )
		{
			JOptionPane.showMessageDialog(null,"Bad input :");
			return;
		}
		
		Polynomial p1 = new Polynomial(input1);
		Polynomial p3 = new Polynomial();
		
		p3 = p1.integratePoly();
	    output = p3.convertPolyString2();
	}

	public void setValue(String value) {
		output = value;
	}

	public String getValue() {
		return output;
	}
}
