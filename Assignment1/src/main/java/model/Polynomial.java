package model;
import java.text.DecimalFormat;
import java.util.*;
/**
 * @author danimodoc Clasa Polynomial contine o lista de monoame pentru a construi un polinom .
 */
public class Polynomial {
	protected ArrayList<Monomial> List = new ArrayList<Monomial>();
	protected int polygrad;

	public Polynomial() {
	}

	public Polynomial(String s) { // Constructorul clasei Polynomial , va construi un polinom din stringul primit ca parametru
		String[] parts = s.split(" ");
		ArrayList<String> nums = new ArrayList<String>();
		int nr = 0;
		for (String x : parts) {
			String[] parts2 = x.split("x\\^");
			for (String x2 : parts2) {
				nums.add(x2);
				nr++;
			}
		}
		for (int i = 0; i < nr; i += 2) {
			Monomial m = new Monomial();
			m.coef = Integer.parseInt(nums.get(i));
			m.grad = Integer.parseInt(nums.get(i + 1));
			if (m.coef != 0)
				this.List.add(m);
		}
		Collections.sort(this.List);
	}

	@Override
	public String toString() {// Metoda realizeaza conversia in string a listei unui polinom
		String p = new String("");
		for (Monomial m : this.List)
			if (m.coef > 0)
				p = p + "+" + (int) m.coef + "x^" + m.grad + " ";
			else if (m.coef < 0)
				p = p + (int) m.coef + "x^" + m.grad + " ";
		return p;
	}

	protected String convertPolyString2() {
		//final  DecimalFormat df2 = new DecimalFormat(".##");
		String p = new String("");
		for (Monomial m : this.List)
			if (m.coef > 0)
				p = p + "+" + m.coef + "x^" + m.grad + " ";
			else if (m.coef < 0)
				p = p + m.coef + "x^" + m.grad + " ";
		return p;
	}

	protected Polynomial addPoly1(Polynomial p) {// Metoda realizeaza adunarea a 2 polinoame addPoly1 daca primul polinom are grad mai mare , altfel addPoly2
		Polynomial newp = new Polynomial();
		for (Monomial m : this.List) {
			int count = 0;
			Monomial m3 = new Monomial();
			for (Monomial m2 : p.List) {
				if (m.grad == m2.grad) {
					m3 = m.addMono(m2);
					newp.List.add(m3);
					break;
				}
				count++;
			}
			if (count == p.List.size())
				newp.List.add(m);
		}
		for (Monomial m : p.List) {
			int count = 0;
			for (Monomial m2 : this.List) {
				if (m.grad == m2.grad)
					break;
				count++;
			}
			if (count == this.List.size())
				newp.List.add(m);
		}
		Collections.sort(newp.List);
		return newp;
	}

	protected Polynomial addPoly2(Polynomial p) {
		Polynomial newp = new Polynomial();
		for (Monomial m : p.List) {
			int count = 0;
			Monomial m3 = new Monomial();
			for (Monomial m2 : this.List) {
				if (m.grad == m2.grad) {
					m3 = m.addMono(m2);
					newp.List.add(m3);
					break;
				}
				count++;
			}
			if (count == this.List.size()) 
				newp.List.add(m);
		}
		for (Monomial m : this.List) {
			int count = 0;
			for (Monomial m2 : p.List) {
				if (m.grad == m2.grad)
					break;
				count++;
			}
			if (count == p.List.size())
				newp.List.add(m);
		}
		Collections.sort(newp.List);
		return newp;
	}

	protected Polynomial reverse() {
		ArrayList<Monomial> auxlist = new ArrayList<Monomial>();
		for (Monomial m : this.List) {
			double aux = m.coef * (-1);
			Monomial m2 = new Monomial(aux, m.grad);
			auxlist.add(m2);
		}
		Collections.sort(auxlist);
		this.List = auxlist;
		return this;
	}

	protected Polynomial subPoly1(Polynomial p) {// Metoda realizeaza scaderea a 2 polinoame , foloseste metoda reverse() care inmulteste un polinom cu -1 ,subPoly1 daca primul polinom are grad mai mare , altfel subPoly2
		Polynomial newp = new Polynomial();
		Polynomial p2 = p.reverse();
		if (this.List.size() > p2.List.size()) {
			for (Monomial m : this.List) {
				int count = 0;
				Monomial m3 = new Monomial();
				for (Monomial m2 : p2.List) {
					if (m.grad == m2.grad) {
						m3 = m.addMono(m2);
						newp.List.add(m3);
						break;
					}
					count++;
				}
				if (count == p2.List.size())
					newp.List.add(m);
			}
			for (Monomial m : p.List) {
				int count = 0;
				for (Monomial m2 : this.List) {
					if (m.grad == m2.grad)
						break;
					count++;
				}
				if (count == this.List.size())
					newp.List.add(m);
			}
		}
		p.reverse();
		Collections.sort(newp.List);
		return newp;
	}

	protected Polynomial subPoly2(Polynomial p) {
		Polynomial newp = new Polynomial();
		Polynomial p2 = p.reverse();
		for (Monomial m : p2.List) {
			int count = 0;
			Monomial m3 = new Monomial();
			for (Monomial m2 : this.List) {
				if (m.grad == m2.grad) {
					m3 = m.addMono(m2);
					newp.List.add(m3);
					break;
				}
				count++;
			}
			if (count == this.List.size())
				newp.List.add(m);
		}
		for (Monomial m : this.List) {
			int count = 0;
			for (Monomial m2 : p.List) {
				if (m.grad == m2.grad)
					break;
				count++;
			}
			if (count == p.List.size())
				newp.List.add(m);
		}
		p.reverse();
		Collections.sort(newp.List);
		return newp;
	}

	protected Polynomial derivative() {// Se deriveaza primul polinom
		ArrayList<Monomial> auxlist = new ArrayList<Monomial>();
		for (Monomial m : this.List) {
			double aux1 = m.coef * m.grad;
			int aux2 = m.grad - 1;
			Monomial m2 = new Monomial(aux1, aux2);
			auxlist.add(m2);
		}
		this.List = auxlist;
		return this;
	}

	protected Polynomial timesPoly(Polynomial p) {// Metoda realizeaza inmultirea celor 2 polinoame
		ArrayList<Polynomial> polyList = new ArrayList<Polynomial>();
		for (Monomial m : this.List) {
			Polynomial aux = new Polynomial();
			for (Monomial m2 : p.List) {
				Monomial m3 = m.times(m2);
				aux.List.add(m3);
			}
			polyList.add(aux);
		}
		Polynomial aux2 = polyList.get(0);
		for (int i = 1; i < polyList.size(); i++) {
			if (polyList.get(i).List.get(0).getGrad() > aux2.List.get(0).getGrad())
				aux2 = aux2.addPoly1(polyList.get(i));
			else
				aux2 = aux2.addPoly2(polyList.get(i));
		}
		return aux2;
	}

	protected Polynomial integratePoly() {// Se integreaza primul polinom
		Polynomial p = new Polynomial();
		for (Monomial m : this.List) {
			m.integrate();
			p.List.add(m);
		}
		return p;
	}

	protected boolean isZero() { // Se verifica daca polinomul este nul
		int count = 0;
		for (Monomial m : this.List)
			if (m.coef == 0)
				count++;
		if (count == this.List.size())
			return true;
		return false;
	}

	protected int getGrad() {// Metoda returneaza gradul polinomului
		for (Monomial m : this.List)
			if (m.coef != 0)
				return m.grad;
		return 0;
	}

	protected Monomial getHighestDegMonome() {// Metoda returneaza monomul cu gradul cel mai mare
		for (Monomial m : this.List)
			if (m.coef != 0)
				return m;
		return null;
	}

	protected Polynomial[] divide(Polynomial d) {// Impartirea a doua polinoame ,ultima metoda
		if (d.isZero())
			return null;
		Polynomial q = new Polynomial();
		Polynomial r = new Polynomial();
		for (Monomial m1 : this.List) {
			r.List.add(m1);
		}
		Monomial t = new Monomial();
		Polynomial aux = new Polynomial();
		while (!r.isZero() && (r.getGrad() >= d.getGrad())) {
			Polynomial T = new Polynomial();
			t = r.getHighestDegMonome().divideBy(d.getHighestDegMonome());
			T.List.add(t);
			if (q.getGrad() > T.getGrad())
				q = q.addPoly1(T);
			else
				q = q.addPoly2(T);
			aux = T.timesPoly(d);
			if (r.getGrad() > aux.getGrad())
				r = r.subPoly1(aux);
			else
				r = r.subPoly2(aux);
		}
		return new Polynomial[] { q, r };
	}
}
