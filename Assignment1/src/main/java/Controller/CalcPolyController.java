package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import View.CalcPolyView;
import model.CalcPolyModel;

public class CalcPolyController {
	private CalcPolyModel m_model;
	private CalcPolyView m_view;
	
	public CalcPolyController(CalcPolyModel model, CalcPolyView view) {
		m_model = model;
		m_view = view;
		
   //Adaugarea de listeneri pentru view
		view.addListener(new AddListener());
		view.subListener(new SubListener());
		view.multiplyBtnListener(new MultListener());
		view.diffBtnListener(new DiffListener());
		view.integBtnListener(new IntegListener());
		view.divideBtnListener(new divListener());
	}
	
	class AddListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			String userInput2 = "";
			try {
				userInput1 = m_view.getUserInput1();
				userInput2 = m_view.getUserInput2();
				
				m_model.addInputs(userInput1, userInput2);
				m_view.setTotal(m_model.getValue());
				
			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'" + userInput2);
			}
		}
	}
	
	class SubListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			String userInput2 = "";
			try {
				userInput1 = m_view.getUserInput1();
				userInput2 = m_view.getUserInput2();
				
				m_model.subInputs(userInput1, userInput2);
				m_view.setTotal(m_model.getValue());
				

			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'" + userInput2);
			}
		}
	}
	
	class MultListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			String userInput2 = "";
			try {
				userInput1 = m_view.getUserInput1();
				userInput2 = m_view.getUserInput2();
				
				m_model.multInputs(userInput1, userInput2);
				m_view.setTotal(m_model.getValue());
				
			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'" + userInput2);
			}
		}
	}
	
	class divListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			String userInput2 = "";
			try {
				userInput1 = m_view.getUserInput1();
				userInput2 = m_view.getUserInput2();
				
				m_model.divInputs(userInput1, userInput2);
				m_view.setTotal(m_model.getValue());
				
			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'" + userInput2);
			}
		}
	}
	
	class DiffListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			try {
				userInput1 = m_view.getUserInput1();
				
				m_model.diffInput(userInput1);
				m_view.setTotal(m_model.getValue());
				
			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'");
			}
		}
	}
	
	class IntegListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String userInput1 = "";
			try {
				userInput1 = m_view.getUserInput1();
				
				m_model.integInput(userInput1);
				m_view.setTotal(m_model.getValue());
				
			} catch (NumberFormatException nfex) {
				m_view.showError("Bad input: '" + userInput1 + "'");
			}
		}
	}
}
