package model;

import org.junit.Test;

import junit.framework.TestCase;

public class JunitTestCase extends TestCase {
	
	
		Polynomial p1=new Polynomial("1x^2 -2x^1 +1x^0");
		Polynomial p2=new Polynomial("1x^1 -1x^0");
		//Test metode din Polynomial
		
		@Test
		public void testConvertPolyString() {
			String s= p1.toString();
			assertEquals("+1x^2 -2x^1 +1x^0 ",s);
		}
		
		@Test
		public void testaddPoly1() {
			Polynomial p=p1.addPoly1(p2);
			String s=p.toString();
			assertEquals("+1x^2 -1x^1 ",s);
		}
		
		@Test
		public void testaddPoly2() {
			Polynomial p=p2.addPoly2(p1);
			String s=p.toString();
			assertEquals("+1x^2 -1x^1 ",s);
		}
		
		@Test
		public void testsubPoly1() {
			Polynomial p=p1.subPoly1(p2);
			String s=p.toString();
			assertEquals("+1x^2 -3x^1 +2x^0 ",s);
		}
		
		@Test
		public void testsubPoly2() {
			Polynomial p=p2.subPoly2(p1);
			String s=p.toString();
			assertEquals("-1x^2 +3x^1 -2x^0 ",s);
		}
		
		@Test
		public void testTimesPoly() {
			Polynomial p=p1.timesPoly(p2);
			String s=p.toString();
			assertEquals("+1x^3 -3x^2 +3x^1 -1x^0 ",s);
		}
		
		@Test
		public void testDerivativePoly() {
			Polynomial p=p1.derivative();
			String s=p.toString();
			assertEquals("+2x^1 -2x^0 ",s);
		}
		
		@Test
		public void testIntegratePoly() {
			Polynomial p=p2.integratePoly();
			String s=p.convertPolyString2();
			assertEquals("+0.5x^2 -1.0x^1 ",s);
		}
		
		@Test
		public void testDividePoly() {
			Polynomial[] p=p1.divide(p2);
			String s1=p[0].convertPolyString2();
			
			assertEquals("+1.0x^1 -1.0x^0 ",s1);
		}
		
		@Test
		public void testGetHighestDegMonome() {
			Monomial m = p1.getHighestDegMonome();
			int[] v= {1,2};
			int[] v2= {(int)m.coef,m.grad};
			assertEquals(v[0],v2[0]);
			assertEquals(v[1],v2[1]);
		}
		
		@Test
		public void testgetGrad() {
			int n=p1.getGrad();
			assertEquals(n,2);
		}
		
		@Test
		public void testIsZero() {
			assertTrue(!p1.isZero());
		}
		
		//Test metode din Monomial
		Monomial m=new Monomial(2,3);
		Monomial n=new Monomial(5,3);
		
		
		@Test
		public void testAddMono() {
			Monomial p=m.addMono(n);
			assertEquals((int)p.coef,7);
		}
		
		@Test
		public void testSubMono() {
			Monomial p=m.subMono(n);
			assertEquals((int)p.coef,-3);
		}
		
		@Test
		public void testTimesMono() {
			Monomial p=m.times(n);
			assertEquals((int)p.coef,10);
			assertEquals(p.grad,6);
		}
		
		@Test
		public void testDivideMono() {
			Monomial p=m.divideBy(n);
			Double x=0.4;
			assertEquals((Double)p.coef,x);
			assertEquals(p.grad,0);
		}
		
		@Test
		public void testIntegrate() {
			m.integrate();
			Double x=0.5;
			assertEquals((Double)m.coef,x);
			assertEquals(m.grad,4);
			
	}


}
